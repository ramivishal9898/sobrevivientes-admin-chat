import * as firebase from "firebase";
import "firebase/database";

let config = {
    apiKey: "AIzaSyDf9OVl9qryH2H52nN8G7WBjzkGIi5iTEw",
    authDomain: "sobrevivientes-66aeb.firebaseapp.com",
    databaseURL: "https://sobrevivientes-66aeb-default-rtdb.firebaseio.com",
    projectId: "sobrevivientes-66aeb",
    storageBucket: "sobrevivientes-66aeb.appspot.com",
    messagingSenderId: "795731393496",
    appId: "1:795731393496:web:d159f65d0976f701cd50ad",
    measurementId: "G-3N7GBKD89M"
};

firebase.initializeApp(config);

export default firebase.database();